extern crate discord_rpc_client;
extern crate mpvipc;
extern crate regex;
extern crate chrono;

use std::{
    fmt::Display,
    clone::Clone,
    path::PathBuf,
    thread,
    time,
    fs,
    env
};

use chrono::{
    prelude::*,
    Duration
};
use regex::{
    Regex,
    Error as RegexError,
};
use mpvipc::{
    Mpv as MPV,
    Event as MpvEvent,
    Error as MpvError,
};
use discord_rpc_client::{
    prelude::SetActivity,
    Client as Discord,
    UnixConnection
};


fn socket_path() -> PathBuf {
    let path = env::var("XDG_RUNTIME_DIR")
        .unwrap_or_else(|_| "/tmp".to_string());
    PathBuf::from(path).join("mpv.socket")
}

fn parse_filename(raw: String) -> Result<(String, String), RegexError> {
    let title_regex = Regex::new(r#"^.*\] (.*) - (\d+).*$"#)?;
    match title_regex.captures(&raw) {
        Some(captures) => Ok((captures[1].to_string(), captures[2].to_string())),
        None => Ok(("Unknown".to_string(), "X".to_string()))
    }
}

fn update_discord_presence<S>(client: &mut Discord<UnixConnection>, status: S, message: S, time_remain: i64)
    where S: Into<String> + Clone + Display
{
    println!(r#"Updating presence to: "{}" with message "{}""#, &status, &message);

    let mut activity = SetActivity::new()
        .state(message)
        .details("Watching Video")
        .assets(|ass| ass
            .large_image("mpv")
            .small_image(status.clone())
            .small_text(status.clone()));

    if status.into() == "playing" {
        activity = activity.timestamps(|t| t.end(time_remain as u32));
    }

    if let Err(_) = client.set_activity(|_| activity) {
        println!("Failed to set presence");
    };
}

fn update_status(discord: &mut Discord<UnixConnection>, mpv: &mut MPV, status: &str) -> Result<(), MpvError> {
    let media_title = mpv.get_property_string("media-title")?;
    let filename = mpv.get_property_string("filename")?;
    let time_remaining = mpv.get_property::<f64>("time-remaining")?;
    let remain = Local::now() + Duration::seconds(time_remaining as i64);
    let message =
        if media_title == filename {
            match parse_filename(filename) {
                Ok((title, episode)) => format!("{} episode {}", title, episode),
                Err(_) => media_title
            }
        } else {
            media_title
        };

    update_discord_presence(discord, status, &message, remain.timestamp());
    Ok(())
}

fn connect(path: PathBuf, discord: &mut Discord<UnixConnection>) -> (MPV, bool) {
    println!("Waiting for MPV...");
    loop {
        match MPV::connect(path.to_str().unwrap()) {
            Err(_) => {
                // println!("Socket down, waiting 10 seconds before retry...");
                thread::sleep(time::Duration::from_secs(10));
            },
            Ok(mut mpv) => {
                println!("Connected to MPV");
                match mpv.get_property::<bool>("pause") {
                    Ok(true) => update_status(discord, &mut mpv, "paused").unwrap(),
                    Ok(false) => update_status(discord, &mut mpv, "playing").unwrap(),
                    Err(_) => (),
                };
                return (mpv, true);
            }
        }
    }
}

fn disconnect(mpv: &mut MPV) -> bool {
    mpv.disconnect();
    fs::remove_file(socket_path()).unwrap();
    println!("Disconnected from MPV");
    false
}

fn main() {
    let mut discord: Discord<UnixConnection> = Discord::new(427458515284656128)
        .and_then(|rpc| rpc.start())
        .expect("Failed to start Discord RPC client");


    let (mut mpv, mut connected) = connect(socket_path(), &mut discord);

    loop {
        if connected {
            match mpv.event_listen() {
                Err(_) => (),
                Ok(MpvEvent::EndFile) => {
                    let remain = Local::now().timestamp();
                    update_discord_presence(&mut discord, "stopped", "Not Playing", remain);
                    connected = disconnect(&mut mpv);
                },
                Ok(MpvEvent::Unpause) => update_status(&mut discord, &mut mpv, "playing").unwrap(),
                Ok(MpvEvent::Pause) => update_status(&mut discord, &mut mpv, "paused").unwrap(),
                Ok(_) => (),
            }
        } else {
            let result = connect(socket_path(), &mut discord);
            mpv = result.0;
            connected = result.1;
        }
    };
}
