# Discord MPV Presence

Display "now playing" information as Discord Rich Presence.

Requires MPV option `input-ipc-server` to be set set to
either `/tmp/mpv.socket` or `$XDG_RUNTIME_DIR/mpv.socket`.

Only works with files matching the regular expression `^.* (\w+) - (\d+).*$`.

For example:
    `[HorribleSubs] Citrus - 09 [1080].mkv`
